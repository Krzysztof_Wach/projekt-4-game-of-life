from tkinter import *

#ramy
master = Tk()

menu = Frame(master, width = 500)
menu.pack(side= LEFT)

okno1 = Frame(master)
okno1.pack(side=RIGHT)
okno = Frame(okno1)
okno.grid()

#siatka
def kolorZielony(rzad, kolumna):
    siatka[kolumna][rzad]['bg']= 'green'
    siatka[kolumna][rzad]['command']= lambda rzad=rzad, kolumna=kolumna: kolorBialy(rzad, kolumna)


def kolorBialy(rzad, kolumna):
    siatka[kolumna][rzad]['bg']= 'white'
    siatka[kolumna][rzad]['command']= lambda rzad=rzad, kolumna=kolumna: kolorZielony(rzad, kolumna)

siatka = [[Button(okno, bg = 'white', height=1, width=2, command = lambda rzad=rzad, kolumna=kolumna: kolorZielony(rzad, kolumna)) for rzad in range(35)] for kolumna in range(35)]
for kolumna in range(35):
    for rzad in range(35):
        siatka[kolumna][rzad].grid(column = kolumna, row = rzad)

#loop
loop = None
def start():
    global loop
    for kolumna in range(35):
        for rzad in range(35):
            siatka[kolumna][rzad]['state']= 'disabled'

    przycisk_start.config(text = 'stop', command = stop)
    spiszKomurki()
    loop = menu.after(500, start)

def stop():
    global loop
    for kolumna in range(35):
        for rzad in range(35):
            siatka[kolumna][rzad]['state']= 'normal'

    przycisk_start.config(text='start', command=start)
    menu.after_cancel(loop)
    loop = None


przycisk_start = Button(menu, text = 'start', command = start)

#reguły
#s = [2,3]
#b = [3]

def czyLiczba(znak):
    if znak.isdigit() or znak == ' ':
        return True

czy_liczba = menu.register(czyLiczba)

s_nazwa = Label(menu, text = 'S')
s = Entry(menu, width = 10, validate = 'key', validatecommand = (czy_liczba, '%S'))
s.insert(0, '2')
s.insert(1, ' ')
s.insert(2, '3')

b_nazwa = Label(menu, text = 'B')
b = Entry(menu, width = 10, validate = 'key', validatecommand = (czy_liczba, '%S'))
b.insert(0, '3')

def spiszKomurki():
    komurkiSB = []
    wartosc_s = s.get().split()
    wartosc_b = b.get().split()
    #dla kazdej komurki
    for kolumna in range(35):
        for rzad in range(35):
            #sprawdz czy przylegajace sa zywe
            counter = 0
            try:
                if siatka[kolumna - 1][rzad - 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna - 1][rzad]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna - 1][rzad + 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna][rzad - 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna][rzad + 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna + 1][rzad - 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna + 1][rzad]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass
            try:
                if siatka[kolumna + 1][rzad + 1]['bg']== 'green':
                    counter+=1
            except IndexError:
                pass

            #od reguł
            if siatka[kolumna][rzad]['bg']== 'green':
                for liczba in wartosc_s:
                    if counter == int(liczba):
                        komurkiSB.append(siatka[kolumna][rzad])
            elif siatka[kolumna][rzad]['bg']!= 'green':
                for liczba in wartosc_b:
                    if counter == int(liczba):
                        komurkiSB.append(siatka[kolumna][rzad])
    ewolucja(komurkiSB)

def ewolucja(komurkiSB):
    #przejdz po wszystkich komurkach
    for kolumna in range(35):
        for rzad in range(35):
            #jezeli komurka na liscie zyjacych, pokoloruj na zielono
            if siatka[kolumna][rzad] in komurkiSB:
                siatka[kolumna][rzad]['bg'] = 'green'
            else:
                siatka[kolumna][rzad]['bg'] = 'white'


s_nazwa.pack()
s.pack()
b_nazwa.pack()
b.pack()
przycisk_start.pack()
przycisk_krok = Button(menu, text = 'krok', command = spiszKomurki).pack()
okno.mainloop()